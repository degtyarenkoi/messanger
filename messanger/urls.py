from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.google_auth),
    url(r'^fb/$', views.fb_auth),
    url(r'^vk/$', views.vk_auth),
    url(r'^exit/$', views.exit),
    url(r'^messages/$', views.messages),
    url(r'^delmess/(?P<message_id>\d+)/$', views.delmess),
    url(r'^delcomm/(?P<comment_id>\d+)/$', views.delcomm),
    url(r'^delrepl/(?P<replication_id>\d+)/$', views.delrepl),
]
