from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User

from datetime import datetime

# Create your models here.

class Message(models.Model):
    class Meta():
        db_table = 'message'
    message = models.CharField(max_length=200)
    time = models.DateTimeField(default=datetime.now, blank=True)
    user = models.ForeignKey(User)

class Comment(models.Model):
    class Meta():
        db_table = 'comment'
    comment = models.CharField(max_length=200)
    time = models.DateTimeField(default=datetime.now, blank=True)
    message = models.ForeignKey(Message)

class Replication(models.Model):
    class Meta():
        db_table = 'replication'
    replication = models.CharField(max_length=200)
    time = models.DateTimeField(default=datetime.now, blank=True)
    message_id = models.IntegerField(default=0)
    comment = models.ForeignKey(Comment)


