from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

import urllib2
import requests
import md5
from datetime import datetime

from django.contrib.auth.models import User
from messanger.models import Message
from messanger.models import Comment
from messanger.models import Replication
from django.contrib.auth import authenticate, login, logout

# Create your views here.

CLIENT_ID = '810999809566-72vf8dq3616ouqog9b79775emtha3lpg.apps.googleusercontent.com'
CLIENT_SECRET = 'DeNj3XEaDRptGGjhE2HOucWq'
REDIRECT_URI = 'http://127.0.0.1:8000'

CLIENT_SECRET_FB = 'e09df903a3b2c8bba23ba2bbe4359f53'
APP_TOKEN_FB = '1750834195204961|AvmApBkEM5-PEfHMCSJRWIJW3uY'
CLIENT_ID_FB = '1750834195204961'
REDIRECT_URI_FB = 'http://127.0.0.1:8000/fb'

CLIENT_SECRET_VK = 'nsjLwbPukDxjPs53cNm0'
CLIENT_ID_VK = '5530897'
REDIRECT_URI_VK = 'http://127.0.0.1:8000/vk'

def google_auth(request):
    user = request.user
    if user.is_anonymous():
        try:  
            code = request.GET.get('code') 
             
            google_post_request = requests.post('https://accounts.google.com/o/oauth2/token', data={'code':code, 'client_id':CLIENT_ID, 'client_secret':CLIENT_SECRET, 'redirect_uri':REDIRECT_URI, 'grant_type':'authorization_code'})
            
            access_token = google_post_request.json()['access_token']        

            google_get_request = requests.get('https://www.googleapis.com/oauth2/v1/userinfo', params={'access_token':access_token})

            user_id = google_get_request.json()['id']
            user_email = google_get_request.json()['email']
            user_name = google_get_request.json()['name'] + ' G+'
            password = md5.new(user_id).digest()

            if not(User.objects.filter(email=user_email).first()):
                user = User.objects.create_user(user_name, user_email, password)
                user = authenticate(username=user_name, password=password)
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        return redirect('/messages/')
            else:
                user = authenticate(username=user_name, password=password)
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        return redirect('/messages/')
            return render_to_response('entries.html', {'user':user}, RequestContext(request))
        except:
            return render_to_response('entries.html', {}, RequestContext(request))
    else:
        return redirect('/messages/')

def fb_auth(request):
    user = request.user
    if user.is_anonymous():
        try:
            code = request.GET.get('code')
            f = requests.get('https://graph.facebook.com/v2.3/oauth/access_token', params={'client_id':CLIENT_ID_FB, 'redirect_uri':REDIRECT_URI_FB, 'client_secret':CLIENT_SECRET_FB, 'code':code})
            access_token = f.json()['access_token']
            user = requests.get('https://graph.facebook.com/debug_token', params={'input_token':access_token, 'access_token':APP_TOKEN_FB})
            user_id = user.json()['data']['user_id']
            user_details = requests.get('https://graph.facebook.com/v2.6/'+user_id, params={'access_token':access_token})
            user_name = user_details.json()['name'] + ' facebook'
            password = md5.new(user_id).digest()

            if not(User.objects.filter(username=user_name).first()):
                user = User.objects.create_user(username=user_name, password=password)
                user = authenticate(username=user_name, password=password)
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        return redirect('/messages/')
            else:
                user = authenticate(username=user_name, password=password)
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        return redirect('/messages/')
            return render_to_response('entries.html', {'user':user}, RequestContext(request))
        except:
            return render_to_response('entries.html', {}, RequestContext(request))
    else:
        return redirect('/messages/')


def vk_auth(request):
    user = request.user
    if user.is_anonymous():
        try:
            code = request.GET.get('code')
            vk = requests.get('https://oauth.vk.com/access_token', params={'client_id':CLIENT_ID_VK, 'client_secret':CLIENT_SECRET_VK, 'code':code, 'redirect_uri':REDIRECT_URI_VK})
            access_token = vk.json()['access_token']
            user_id = vk.json()['user_id']

            fields = 'first_name,last_name'
            user_info = requests.get('https://api.vk.com/method/users.get',params={'uids':user_id, 'fields':fields, 'access_token':access_token})
            user_name =  user_info.json()['response'][0]['first_name'] + ' ' + user_info.json()['response'][0]['last_name'] + ' VK'
            uid = user_info.json()['response'][0]['uid']
            #password = md5.new(uid).digest()
            password = 'vk'
            
            if not(User.objects.filter(username=user_name).first()):
                user = User.objects.create_user(username=user_name, password=password)
                user = authenticate(username=user_name, password=password)
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        return redirect('/messages/')
            else:
                user = authenticate(username=user_name, password=password)
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        return redirect('/messages/')
            return render_to_response('entries.html', {'user':user}, RequestContext(request))
        except:
            return render_to_response('entries.html', {}, RequestContext(request))
    else:
        return redirect('/messages/')

    
def exit(request):
    logout(request)
    return redirect('/')

def messages(request):
    time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    if (request.method == 'POST'):
        message = request.POST.get('message')
        obj = Message.objects.create(message=message, time=time, user_id=request.user.id)
        return redirect('/messages/')
    if (request.method == 'GET'):
        if request.GET.get('comment_id'):
            replication = request.GET.get('replication')
            comment_id = request.GET.get('comment_id') 
            message_id = request.GET.get('message_id')        
            if (replication):
                obj_repl = Replication.objects.create(replication=replication, time=time, message_id=message_id, comment_id=comment_id)
                return redirect('/messages/')
        else:
            comment = request.GET.get('comment')
            message_id = request.GET.get('message_id')
            if (comment):
                obj_comm = Comment.objects.create(comment=comment, time=time, message_id=message_id)
                return redirect('/messages/')
    all_messages = Message.objects.order_by('id').reverse()
    all_comments = Comment.objects.all()
    all_replications = Replication.objects.all()
    return render_to_response('messages.html', {'messages':all_messages, 'comments':all_comments, 'replications':all_replications}, RequestContext(request))

def delmess(request, message_id):
    message = Message.objects.get(id=message_id)
    message.delete()
    comment = Comment.objects.filter(message_id=message_id)
    comment.delete()
    replication = Replication.objects.filter(comment_id=message_id)
    replication.delete()
    return redirect('/messages/')

def delcomm(request, comment_id):
    comment = Comment.objects.filter(id=comment_id)
    comment.delete()
    replication = Replication.objects.filter(comment_id=comment_id)
    replication.delete()
    return redirect('/messages/')

def delrepl(request, replication_id):
    replication = Replication.objects.get(id=replication_id)
    replication.delete()
    return redirect('/messages/')









